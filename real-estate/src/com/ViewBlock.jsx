import React, { useEffect, useState } from "react";
import { FaEye } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/all";
import ReactPaginate from 'react-paginate';
import ReactDOM from 'react-dom';
// import BlockCreationjs from "./Services/BlockCreation";
import blockCreation from "./Services/BlockCreation";
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";

const ViewBlock = () => {

    const [blocks, setBlocks] = useState([])

    const getBlocks = () => {
        blockCreation.getAllBlocks().then((response) => {
            console.log("My Fetched data", response.responseDto)
            setBlocks(response.responseDto.payload)
        })
            .catch(function (ex) {
                console.log('Response parsing failed. Error: ', ex);
            });;
    };

    useEffect(() => {
        getBlocks()
    }, [])



    function handleChange(e) {
        console.log("GetValue", e.target.value)
        console.log("GetValue", e.target.name)
    }

    return (
        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
                <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <table class="styled-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Block Number</th>
                                <th>Block Extent</th>
                                <th>Selling Price Per Perch</th>
                                <th>Block Selling Price Budgeted</th>
                                <th>RTotal Block Cost</th>
                                <th>Block Profit</th>
                                <th>Block Outing Date</th>
                                <th>Available For Sale From</th>
                                <th>Description</th>
                                <th>Created User</th>
                                <th>Created Date</th>
                                <th>Project Name</th>
                                <th>Block Sales Status</th>
                                <th>Create Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {blocks.map(blocks => (
                                <tr>
                                    <td>{blocks.id}</td>
                                    <td>{blocks.blockLotNo}</td>
                                    <td>{blocks.blockExtent}</td>
                                    <td>{blocks.sellingPricePerPerch}</td>
                                    <td>{blocks.blockSellingPriceBudgeted}</td>
                                    <td>{blocks.totalBlockCost}</td>
                                    <td>{blocks.blockProfit}</td>
                                    <td>{blocks.blockOutingDate}</td>
                                    <td>{blocks.availableForSaleFrom}</td>
                                    <td>{blocks.description}</td>
                                    <td>{blocks.createdUser}</td>
                                    <td>{blocks.createDate}</td>
                                    <td>{blocks.reProjectDetailsDto.projectName}</td>
                                    <td>{blocks.reBlockSalesStatusDto.statusName}</td>
                                    <td>{blocks.createTime}</td>
                                    <td style={{ width: '100px' }}>
                                        <button class="table-button">
                                            <FaEye class="table-icon" style={{color:"green"}}/>
                                        </button>
                                        <button class="table-button" style={{ float: 'right' }}>
                                            <RiDeleteBin6Line class="table-icon" style={{color:"red"}}/>
                                        </button>
                                    </td>
                                </tr>
                            ))
                            }
                        </tbody>
                    </table>
                </div>
            </div >
        </div>
    )
};
export default ViewBlock;