import React, { useEffect, useState } from "react";
// import LoginService from "./Services/LoginService";
// import { useParams } from 'react-router-dom';
import Axios from "axios";
import Popup from 'reactjs-popup';
import { useHistory } from "react-router-dom";
import { AppString } from "./Const";

const Login = () => {

    const history = useHistory();
     const url = "http://localhost:8080/user/loginAuth"
    // const url = "http://macs-mf-svr.info:4040/summit_capital/resources/api/login"
    const [data, setData] = useState({
        email: "",
        password: "",
    })
    useEffect(() => {
        if (localStorage.getItem(AppString.USER) == 'true') {
            history.push("/dashboard");
        }
    }, [])

    function submit(e) {
        e.preventDefault();
        Axios.post(url, {
            email: data.email,
            password: data.password,
        })
            .then(res => {
                console.log("Log", res.data.responseDto)
                if (res.data.responseDto) {
                    console.log("True")
                    history.push("/dashboard");
                    localStorage.setItem(AppString.USER, "true");
                }
                else {
                    alert("Invalide Login!!!")
                }
            })
    }

    function handel(e) {
        const newdata = { ...data }
        newdata[e.target.id] = e.target.value
        setData(newdata)
        console.log(newdata)
    }

    return (

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <h1 class="logo-name">RE+</h1>
                </div>
                <h3>Welcome to RE+</h3>

                <p>Login in. To see it in action.</p>
                <form class="m-t" role="form" action="/login" onSubmit={(e) => submit(e)}>
                    <div class="form-group">
                        <input onChange={(e) => handel(e)} value={data.email} type="text" class="form-control" placeholder="Username" required id="email" name="email" />
                    </div>
                    <div class="form-group">
                        <input onChange={(e) => handel(e)} value={data.password} type="password" class="form-control" placeholder="Password" required id="password" name="password" />
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    {/* <Popup trigger={}>
                        <div style={{ height: '100px', width: "200px", backgroundColor: "purple" }}>Hi</div>
                    </Popup> */}
                    {/* <a href="#"><small>Forgot password?</small></a>
                                    <p class="text-muted text-center"><small>Do not have an account?</small></p>
                                    <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> */}
                </form>
            </div>
        </div>
    );
};

export default Login;