import React, { useEffect, useState } from "react";
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";
import Axios from "axios";

const Land = () => {
  const url = "http://localhost:8081/land/save";
  const [data, setData] = useState({
    landName: "",
    landOwner: "",
    totalExtent: "",
    pricePerPerch: "",
    saleMethod: "",
    salePrice: "",
  });

  function submit(e) {
    e.preventDefault();
    Axios.post(url, {
      landName: data.landName,
      landOwner: data.landOwner,
      totalExtent: data.totalExtent,
      pricePerPerch: data.pricePerPerch,
      saleMethod: data.saleMethod,
      salePrice: data.salePrice,
    }).then((res) => {
      console.log(res.data);
    });
    setData({
      landName: "",
      landOwner: "",
      totalExtent: "",
      pricePerPerch: "",
      saleMethod: "",
      salePrice: "",
    });
  }

  function handel(e) {
    const newdata = { ...data };
    newdata[e.target.id] = e.target.value;
    setData(newdata);
    console.log(newdata);
  }

  const [mul, setMul] = useState();

  useEffect(() => {
    setMul(data.totalExtent * data.pricePerPerch);
  }, [data]);

  return (
    <div>
      <SideNav />
      <div style={{ width: "83%", float: "left" }}>
        <SearchBar />
        <div
          id="page-wrapper"
          class="gray-bg dashbard-1"
          style={{ width: "100%" }}
        >
          <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
              <h2>Create Land</h2>
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="home.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                  <a>Create Land</a>
                </li>
                <li class="breadcrumb-item active">
                  <strong>Create Land</strong>
                </li>
              </ol>
            </div>
          </div>

          <form onSubmit={(e) => submit(e)}>
            <div class="wrapper wrapper-content animated fadeInRight">
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="ibox">
                      <div class="ibox-content">
                        <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">
                            Land Name<span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.landName}
                              type="text"
                              class="form-control form-control-sm"
                              id="landName"
                              name="landName"
                              required
                            />
                          </div>
                          <label for="" class="col-sm-2 col-form-label">
                            Land Owner Name
                            <span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.landOwner}
                              type="text"
                              class="form-control form-control-sm"
                              id="landOwner"
                              name="landOwner"
                              required
                            />
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">
                            Total Extent{" "}
                            <span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.totalExtent}
                              rows="3"
                              type="number"
                              step="any"
                              class="form-control"
                              id="totalExtent"
                              name="totalExtent"
                              required
                            />
                          </div>

                          <label for="" class="col-sm-2 col-form-label">
                            Price per Perch
                            <span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.pricePerPerch}
                              type="number"
                              step="any"
                              class="form-control form-control-sm"
                              id="pricePerPerch"
                              name="pricePerPerch"
                              min="0"
                              required
                            />
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">
                            Sale Method<span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.saleMethod}
                              type="text"
                              class="form-control form-control-sm"
                              id="saleMethod"
                              name="saleMethod"
                              required
                            />
                          </div>
                          <label for="" class="col-sm-2 col-form-label">
                            Sale Price
                          </label>
                          <div class="col-sm-4">
                            <span
                              class="form-control form-control-sm"
                              id="salePrice"
                              name="salePrice"
                              placeholder="hdkj"
                              value={mul}
                            >
                              {mul}
                            </span>
                          </div>
                        </div>
                        <button class="btn btn-primary btn-sm">Save</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Land;
