import React, { useEffect, useState } from "react";
import Axios from "axios";
import SearchBar from "./SearchBar";
import SideNav from "./SideNav";
import projectJsx from "../com/Services/Project";
//import saleTypeConfigurationJs from "../com/Services/SaleTypeCofigurationService";

const SaleTypeConfiguration = () => {
    const url = "http://localhost:8081/salesTypeConfiguration/save"
    const [data, setData] = useState({
        configurationName: "",
        nonRefundableAdvancePercentage: "",
        stage1Percentage: "",
        stage1MaxDays: "",
        stage2Percentage: "",
        stage2MaxDays: "",
        stage3Percentage: "",
        stage3MaxDays: "",
        finalPaymentMaxDays: "",
        //reProjectDetailsDto: "",

    })
    function submit(e) {
        e.preventDefault();
        Axios.post(url, {
            configurationName: data.configurationName,
            nonRefundableAdvancePercentage: data.nonRefundableAdvancePercentage,
            stage1Percentage: data.stage1Percentage,
            stage1MaxDays: data.stage1MaxDays,
            stage2Percentage: data.stage2Percentage,
            stage2MaxDays: data.stage2MaxDays,
            stage3Percentage: data.stage3Percentage,
            stage3MaxDays: data.stage3MaxDays,
            finalPaymentMaxDays: data.finalPaymentMaxDays,
            //reProjectDetailsDto: data.reProjectDetailsDto,
        })
            .then(res => {
                console.log(res.data)
            })
    }

    const [projects, setProjects] = useState([]);

    useEffect(() => {
        getProjects()
    }, [])

    function handel(e) {
        const newdata = { ...data }
        newdata[e.target.id] = e.target.value
        setData(newdata)
        console.log(newdata)
    }
    const getProjects = () => {
        projectJsx.getAllProjects().then((response) => {
            console.log("Project data", response.responseDto)
            setProjects(response.responseDto.payload)
        })
            .catch(function (ex) {
                console.log('Response parsing failed. Error: ', ex);
            });;
    };

    return (
        <div>
            <div>
                <SideNav />
                <div style={{ width: '83%', float: 'left' }}>
                    <SearchBar />
                    <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                        <div class="row wrapper border-bottom white-bg page-heading">
                            <div class="col-lg-10">
                                <h2>Create Sale Types</h2>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="home.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a>Create Sale Types</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                        <strong>Create Sale Types</strong>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <form onSubmit={(e) => submit(e)}>
                        <div class="wrapper wrapper-content animated fadeInRight">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-form-label">Project </label>
                                                    <div class="col-sm-4">
                                                    
                                                        <select  class="select2 form-control" id="project" name="project" onChange={(e) => handel(e)}>
                                                            {projects.map(project =>
                                                                <option >{project.projectName}</option>
                                                            )}
                                          
                                                        </select>
                                                        
                                                    </div>
                                                    <label for="" class="col-sm-1 col-form-label">Sale Type Name</label>
                                                    <div class="col-sm-4">
                                                        <input onChange={(e) => handel(e)} value={data.configurationName}type="text" class="form-control form-control-sm" id="configurationName" name="configurationName" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-form-label">Sale Method</label>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control form-control-sm" id="saleMethod" name="saleMethod" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="" class="col-sm-2 col-form-label">Non Refundable Advance</label>
                                                    <div class="col-sm-2">
                                                        <select class="select2 form-control">
                                                            <option>Select One</option>
                                                            <option>5%</option>
                                                            <option>10%</option>
                                                            <option>15%</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control form-control-sm" />
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-hover table-sm text-center" style={{ width: "50%" }}>
                                                                <thead>
                                                                    <tr>
                                                                        <td >Stage</td>
                                                                        <td>Days</td>
                                                                        <td>%</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody >
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td ><input onChange={(e) => handel(e)} value={data.stage1MaxDays} type="number"  class="form-control form-control-sm" id="stage1MaxDays" name="stage1MaxDays" min="0"  /></td>
                                                                        <td ><input onChange={(e) => handel(e)} value={data.stage1Percentage} type="number" step="any" class="form-control form-control-sm" id="stage1Percentage" name="stage1Percentage" min="0" required /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td ><input onChange={(e) => handel(e)} value={data.stage2MaxDays} type="number" class="form-control form-control-sm" id="stage2MaxDays" name="stage2MaxDays" min="0" required /></td>
                                                                        <td ><input onChange={(e) => handel(e)} value={data.stage2Percentage} type="number" step="any" class="form-control form-control-sm" id="stage2Percentage" name="stage2Percentage" min="0" required /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td ><input onChange={(e) => handel(e)} value={data.stage3MaxDays} type="number" class="form-control form-control-sm" id="stage3MaxDays" name="stage3MaxDays" min="0" required /></td>
                                                                        <td ><input onChange={(e) => handel(e)} value={data.stage3Percentage} type="number" step="any" class="form-control form-control-sm" id="stage3Percentage" name="stage3Percentage" min="0" required /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Final</td>
                                                                        <td  ><input onChange={(e) => handel(e)} value={data.finalPaymentMaxDays} type="number" class="form-control form-control-sm" id="finalPaymentMaxDays" name="finalPaymentMaxDays" min="0" required /></td>
                                                                        <td >100%</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-6">
                                                        <button type="button" class="btn btn-primary btn-sm pull-right" type="submit" value="Submit">Save</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
</form>

                        <div class="footer">
                            <div class="float-right">
                                10GB of <strong>250GB</strong> Free.
                            </div>
                            <div>
                                <strong>Copyright</strong> Example Company &copy; 2014-2018
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default SaleTypeConfiguration;