import React, { useEffect, useState } from "react";
import SearchBar from "./SearchBar";
import SideNav from "./SideNav";
import Axios from "axios";
import projectJsx from "../com/Services/Project";

const BlockOutPlan = () => {
  const [projects, setProjects] = useState([]);
  const [branches, setBranches] = useState([]);
  const [sbus, setSbus] = useState([]);

  useEffect(() => {
    getProjects();
    getBranches();
    getSbus();
  }, []);

  const getProjects = () => {
    projectJsx
      .getAllProjects()
      .then((response) => {
        console.log("Project data", response.responseDto);
        setProjects(response.responseDto.payload);
      })
      .catch(function (ex) {
        console.log("Response parsing failed. Error: ", ex);
      });
  };

  const getBranches = () => {
    projectJsx
      .getAllBranchesList()
      .then((response) => {
        console.log("Branch data", response.responseDto);
        setBranches(response.responseDto.payload);
      })
      .catch(function (ex) {
        console.log("Response parsing failed. Error: ", ex);
      });
  };

  const getSbus = () => {
    projectJsx
      .getAllSBU()
      .then((response) => {
        console.log("SBU data", response.responseDto);
        setSbus(response.responseDto.payload);
      })
      .catch(function (ex) {
        console.log("Response parsing failed. Error: ", ex);
      });
  };

  // const [rows, setRows] = useState(0);
  // const addRow = () => {
  //     setModified(true);
  //     setRows((prevRows) => prevRows + 1);
  //     let array = [""];
  //     for (let i = 1; i < columns.length; i++) {
  //       array.push("");
  //     }
  //     setRowsData((prevRowsData) => [...prevRowsData, array]);
  //   };

  const url = "http://localhost:8081/reProjectDetails/save";
  const [data, setData] = useState({
    projectName: "",
    reservationExtent: "",
    otherReservationsExtent: "",
    roadWaysExtent: "",
    startDate: "",
    endDate: "",
    budgetedCost: "",
    costOfCapital: "",
    budgetedRevenue: "",
    totalExtent: "",
    description: "",
    projectOfficer: "",
    branchDto: undefined,
    sbuDto: undefined,
  });

  function submit(e) {
    e.preventDefault();
    Axios.post(url, {
      projectName: data.projectName,
      reservationExtent: data.reservationExtent,
      otherReservationsExtent: data.otherReservationsExtent,
      roadWaysExtent: data.roadWaysExtent,
      startDate: data.startDate,
      endDate: data.endDate,
      budgetedCost: data.budgetedCost,
      costOfCapital: data.costOfCapital,
      budgetedRevenue: data.budgetedRevenue,
      totalExtent: data.totalExtent,
      description: data.description,
      projectOfficer: data.projectOfficer,
      branchDto: data.branchDto,
      sbuDto: data.sbuDto,
    }).then((res) => {
      console.log(res.data);
    });
  }

  function handel(e) {
    console.log("E", e.target.value);
    const newdata = { ...data };
    newdata[e.target.id] = e.target.value;
    setData(newdata);
    console.log(newdata);
  }

  const [noOfRows, setNoOfRows] = useState(0);
  const [rows, setRows] = useState(1);
  const total = (noOfRows + parseInt(rows));
  console.log("Total : " + total)
  const handleRowChange = ({ target }) => {
    setRows(target.value);
    console.log("Rows : " + rows)
  };

  return (
    <div>
      <SideNav />
      <div style={{ width: "83%", float: "left" }}>
        <SearchBar />
        <div
          id="page-wrapper"
          class="gray-bg dashbard-1"
          style={{ width: "100%" }}
        >
          <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
              <h2>Blocking out the Project</h2>
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="home.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                  <a>Blocking out the Project</a>
                </li>
                <li class="breadcrumb-item active">
                  <strong>Blocking out the Project</strong>
                </li>
              </ol>
            </div>
          </div>

          <div class="wrapper wrapper-content animated fadeInRight">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-12">
                  <div class="ibox">
                    <div class="ibox-content">
                      <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                            <label for="" class="col-form-label">
                              SBU
                            </label>
                            <select class="form-control" id="sbu" name="sbu">
                              <option>--- Select ---</option>
                              {sbus.map((sbu) => (
                                <option
                                  value={sbu.id}
                                  placeholder="Search for something..."
                                >
                                  {sbu.sbuName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div class="col-sm-3">
                          <div class="form-group">
                            <label for="" class="col-form-label">
                              Branch
                            </label>
                            <select
                              class="form-control"
                              id="branch"
                              name="branch"
                            >
                              <option>--- Select ---</option>
                              {branches.map((branch) => (
                                <option value={branch.id}>
                                  {branch.branchName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div class="col-sm-3">
                          <div class="form-group">
                            <label for="" class=" col-form-label">
                              Project
                            </label>
                            <select
                              class="form-control"
                              id="project"
                              name="project"
                            >
                              <option>--- Select ---</option>
                              {projects.map((project) => (
                                <option value={project.id}>
                                  {project.projectName}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        {/* <div class="col-sm-3">
                                                    <div class="form-group ">
                                                        <label for="" class=" col-form-label" style={{ color: "#fff" }}>btn</label><br />
                                                        <button type="button" class="btn btn-info btn-sm">Select</button>
                                                    </div>
                                                </div> */}
                      </div>

                      <hr />

                      <div class="form-group row">
                        <label for="" class="col-sm-1 col-form-label">
                          No of Blocks
                        </label>
                        <div class="col-sm-2">
                          <input
                            type="number"
                            class="form-control form-control-sm"
                            id="blockNo"
                            name="blockNo"
                            min="1"
                            onChange={handleRowChange}
                            // value={adVal}
                                
                          />
                        </div>
                        <div class="col-sm-4">
                          <button
                            type="button"
                            class="btn btn-success btn-sm"
                             onClick={() => setNoOfRows(total)}
                           //onClick={handleClick}
                          >
                            Add
                          </button>{" "}
                          {/*onClick={addRow}*/}
                        </div>
                      </div>
                    </div>

                    <br />
                    {/* <div>
                                            <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="DataTables_Table_0" /></label>
                                            <a class="btn btn-default buttons-copy buttons-html5" tabindex="0" aria-controls="DataTables_Table_0" href="#"><span>Copy</span></a>
                                        </div> */}
                    <div class="table-responsive">
                      <table>
                        {" "}
                        {/*class="table table-striped table-bordered table-hover dataTables-example"*/}
                        <thead>
                          <tr>
                            <th>LOT NO</th>
                            <th>Area</th>
                            <th>Cost Per Perch</th>
                            <th>Sale Price Per Perch</th>
                            <th>Total Sale Price</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          {[...Array(noOfRows)].map((elementInArray, index) => {
                            return (
                              <tr>
                                <th scope="row">{index + 1 == 2? index = 3 : index = index+1}</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                  <button
                                    type="button"
                                    class="btn btn-secondary"
                                  >
                                    Cancel
                                  </button>
                                  <button type="button" class="btn btn-warning">
                                    Edit
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                        <tfoot>
                          <tr>
                            <td>Balance</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>

                    <div class="col-sm-12">
                      <button type="button" class="btn btn-success pull-right">
                        Save
                      </button>
                    </div>
                    <br />
                    <br />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="footer">
            <div class="float-right">
              10GB of <strong>250GB</strong> Free.
            </div>
            <div>
              <strong>Copyright</strong> Example Company &copy; 2014-2018
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlockOutPlan;
