import React from "react";
// import profile_small from "./img/profile_small.jpg"
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";

const receipting = () => {
    return (
        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
                <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <div class="row wrapper border-bottom white-bg page-heading">
                        <div class="col-lg-10">
                            <h2>Receipt</h2>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a>Receipt</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <strong>Receipt</strong>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Sale Transaction</label>
                                                        <input type="text" class="form-control form-control-sm" id="transaction" name="transaction" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" class=" col-form-label">Customer</label>
                                                        <input type="text" class="form-control form-control-sm" id="customer" name="customer" />
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" class=" col-form-label">Project Block</label>
                                                        <select class="select2 form-control" id="projectBlock" name="projectBlock">
                                                            <option>Select One</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label for="" class=" col-form-label" style={{color: "#fff" }}>btn</label><br />
                                                        <button type="button" class="btn btn-success btn-sm ">Load</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-sm" placeholder="Project" disabled="" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-sm" placeholder="LOT" disabled="" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-sm" placeholder="Sale Transaction" disabled="" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-sm" placeholder="Customer" disabled="" />
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-hover table-sm" style={{ width: "50%" }}>
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Charge 1</td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Charge 2</td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Charge 3</td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>LOT</td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                                <td><input type="text" class="form-control form-control-sm" /></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-sm-6">
                                                    <button type="button" class="btn btn-primary pull-right">Save</button>
                                                </div>
                                                <br /><br />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="footer">
                        <div class="float-right">
                            10GB of <strong>250GB</strong> Free.
                        </div>
                        <div>
                            <strong>Copyright</strong> Example Company &copy; 2014-2018
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default receipting;