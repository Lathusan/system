import React from "react";
import Dashboard from "./Dashboard";
import profile_small from "./img/profile_small.jpg"

const SideNav = () => {
    return (

        <div id="wrapper" style={{ width: '17%', float: 'left' }}>
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <img src={profile_small} class="rounded-circle" alt="Profile" />
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="block m-t-xs font-bold">David Williams</span>
                                    <span class="text-muted text-xs block">Art Director <b class="caret"></b></span>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                                    <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                                    <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                    <li class="dropdown-divider"></li>
                                    <li><a class="dropdown-item" href="login.html">Logout</a></li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                RE+
                            </div>
                        </li>
                        <li>
                            <a href="/dashboard"><i class="fa fa-laptop"></i> <span class="nav-label">Dashboard</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">Admin</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/land">New Land</a></li>
                                <li><a href="/branch">New Branch</a></li>
                                <li> <a href="/saleTypeConfiguration"> <span class="nav-label">Sale Type Configuration</span></a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">Project</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/createProject">New Project</a></li>
                                <li><a href="/viewProject"> <span class="nav-label">View Project</span></a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">Block</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/blockOutPlan"><span class="nav-label">Block Out Plan</span></a></li>
                                <li><a href="/viewBlock"><span class="nav-label">View Block</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">Sale</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/blockReservation">Block Sale</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">Cashier</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="/receipting"><span class="nav-label">Receipting</span></a></li>
                                <li><a href="/blockReservationReceipts"><span class="nav-label">Block Reservation Receipts</span></a></li>

                            </ul>
                        </li>
                        <li>
                            <a href="/createCustomer"><i class="fa fa-user"></i> <span class="nav-label">Create Customer</span></a>
                        </li>
                        <li>

                        </li>
                        <li>
                            <a href="/interactionAdding"><i class="fa fa-diamond"></i> <span class="nav-label">Interaction Adding</span></a>
                        </li>
                        <li>
                            <a href="/PaginationTable"><i class="fa fa-diamond"></i> <span class="nav-label">PaginationTable</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

    );
};

export default SideNav;