import React from "react";
import SearchBar from "./SearchBar";
import SideNav from "./SideNav";
// import profile_small from "./img/profile_small.jpg"

const CreateCustomer = () => {
    return (

        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
                <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <div class="row wrapper border-bottom white-bg page-heading">
                        <div class="col-lg-10">
                            <h2>Create Customer</h2>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a>Create Customer</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <strong>Create Customer</strong>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <div class="form-group row">
                                                <label for="" class="col-sm-2 col-form-label">NIC <span style={{ color: "red" }}>*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="nic" name="nic" />
                                                </div>
                                                <label for="" class="col-sm-1 col-form-label">NIC <span style={{ color: "red" }}>*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="nic1" name="nic1" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-2 col-form-label">Names by Initials<span style={{ color: "red" }}>*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="namesByInitials" name="namesByInitials" />
                                                </div>
                                                <label for="" class="col-sm-1 col-form-label">Last Name<span style={{ color: "red" }}>*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="lName" name="lName" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-2 col-form-label">Initials</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="initials" name="initials" disabled="" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-2 col-form-label">DOB</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="dob" name="dob" disabled="" />
                                                </div>
                                                <label for="" class="col-sm-1 col-form-label">Gender</label>
                                                <div class="col-sm-4">
                                                    <select class="form-control form-control-sm" id="gender" name="gender" disabled="">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-2 col-form-label">Address<span style={{ color: "red" }}>*</span></label>
                                                <div class="col-sm-4">
                                                    <textarea rows="3" class="form-control"></textarea>
                                                </div>
                                                <label for="" class="col-sm-1 col-form-label">Mobile<span style={{ color: "red" }}>*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="mobile" name="mobile" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-2 col-form-label">Whatsapp</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="whatsapp" name="whatsapp" />
                                                </div>
                                                <label for="" class="col-sm-1 col-form-label">E-mail</label>
                                                <div class="col-sm-4">
                                                    <input type="email" class="form-control form-control-sm" id="email" name="email" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="footer">
                        <div class="float-right">
                            10GB of <strong>250GB</strong> Free.
                        </div>
                        <div>
                            <strong>Copyright</strong> Example Company &copy; 2014-2018
                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
};

export default CreateCustomer;
{/* script file is miising */ }