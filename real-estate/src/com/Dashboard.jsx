import React, { useEffect, useState } from "react";

import { AppString } from "./Const";
import SideNav from "./SideNav";
import { useHistory } from "react-router-dom";
import SearchBar from "./SearchBar";

const Dashboard = () => {
    const history = useHistory();

    useEffect(() => {
        if (localStorage.getItem(AppString.USER) != 'true') {
            history.push("/");
        }
    }, [])


    return (
        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
            <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <div class="row wrapper border-bottom white-bg page-heading">
                        <div class="col-lg-10">
                            <h2>Dashboard</h2>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>

                                <li class="breadcrumb-item active">
                                    <strong>Dashboard</strong>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="wrapper wrapper-content animated fadeInRight">

                    </div>


                    <div class="footer">
                        <div class="float-right">
                            10GB of <strong>250GB</strong> Free.
                        </div>
                        <div>
                            <strong>Copyright</strong> Example Company &copy; 2014-2018
                        </div>
                    </div>

                </div>
            </div>
        </div>

    );
};

export default Dashboard;