
const PROJECT_API_BASE_URL = "http://localhost:8081/reBlocks/";

class Blocks {   

    getAllBlocks() {
        return fetch(PROJECT_API_BASE_URL + "getAll?pageNumber=1&pageSize=10",{ 
            method: 'get',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }

    saveBlocks(body) {
        return fetch(PROJECT_API_BASE_URL,{ 
            method: 'post',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }
}
export default new Blocks();

