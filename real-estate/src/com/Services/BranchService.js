const PROJECT_API_BASE_URL = "http://localhost:8081/";

class BranchService {

    saveBranch(body) {
        return fetch(PROJECT_API_BASE_URL, "branch/save", {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
            },
            'credentials': 'same-origin'
        })
            .then(res => res.json());
    }

}
export default new BranchService();