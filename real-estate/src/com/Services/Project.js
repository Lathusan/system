// import axios from 'axios';
const PROJECT_API_BASE_URL = "http://localhost:8081/";

class Project {   

    getAllProjects() {
        // return axios.get(PROJECT_API_BASE_URL);
        return fetch(PROJECT_API_BASE_URL + "reProjectDetails/getAll?pageNumber=1&pageSize=0",{ 
            method: 'get',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }

    saveProject(body) {
        return fetch(PROJECT_API_BASE_URL,{ 
            method: 'post',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }

    getAllBranchesList() {
        return fetch(PROJECT_API_BASE_URL + "branch/getAll?pageNumber=1&pageSize=0",{ 
            method: 'get',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }

    getAllSBU() {
        return fetch(PROJECT_API_BASE_URL + "sbu/getAll?pageNumber=1&pageSize=0",{ 
            method: 'get',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }

}
export default new Project();

