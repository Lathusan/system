// import axios from 'axios';
const PROJECT_API_BASE_URL = "http://localhost:8080/user/loginAuth";

class Login {

    checkLogin(body) {
        return fetch(PROJECT_API_BASE_URL,{ 
            method: 'post',
                headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                },
                'credentials': 'same-origin'
        })
        .then(res => res.json());    
    }

}
export default new Login();


// import http from "../CommonHttp";

// class LoginService {
 
//     // const config = {
//     //     headers: {
//     //       "Access-Control-Allow-Origin": "*",
//     //       "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS"
//     //     }
//     //   };

// //     getAll() {
// //     return http.get("/tutorials");
// //   }

// //   get(id) {
// //     return http.get(`/tutorials/${id}`);
// //   }

//   checkLogin(email , password) {
//     return http.post("/login" + email , password);
//   }

// //   update(id, data) {
// //     return http.put(`/tutorials/${id}`, data);
// //   }

// //   delete(id) {
// //     return http.delete(`/tutorials/${id}`);
// //   }

// //   deleteAll() {
// //     return http.delete(`/tutorials`);
// //   }

// //   findByTitle(title) {
// //     return http.get(`/tutorials?title=${title}`);
// //   }
// }

// export default new LoginService();