import React, { useEffect, useState } from "react";
//import createScript from "./internalScript createproject";
import Axios from "axios";
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";
import projectJsx from "../com/Services/Project";

const CreateProject = (props) => {
  const url = "http://localhost:8081/reProjectDetails/save";
  const [data, setData] = useState({
    projectName: "",
    reservationExtent: "",
    otherReservationsExtent: "",
    roadWaysExtent: "",
    startDate: "",
    endDate: "",
    budgetedCost: "",
    costOfCapital: "",
    budgetedRevenue: "",
    totalExtent: "",
    description: "",
    projectOfficer: "",
    branchDto: undefined,
    sbuDto: undefined,
    sellableExtent: "",
  });

  function submit(e) {
    e.preventDefault();
    Axios.post(url, {
      projectName: data.projectName,
      reservationExtent: data.reservationExtent,
      otherReservationsExtent: data.otherReservationsExtent,
      roadWaysExtent: data.roadWaysExtent,
      startDate: data.startDate,
      endDate: data.endDate,
      budgetedCost: data.budgetedCost,
      costOfCapital: data.costOfCapital,
      budgetedRevenue: data.budgetedRevenue,
      totalExtent: data.totalExtent,
      description: data.description,
      projectOfficer: data.projectOfficer,
      branchDto: data.branchDto,
      sbuDto: data.sbuDto,
      sellableExtent: data.sellableExtent,
    }).then((res) => {
      console.log(res.data);
    });
    // setData({
    //   projectName: "",
    //   reservationExtent: "",
    //   otherReservationsExtent: "",
    //   roadWaysExtent: "",
    //   startDate: "",
    //   endDate: "",
    //   budgetedCost: "",
    //   costOfCapital: "",
    //   budgetedRevenue: "",
    //   totalExtent: "",
    //   description: "",
    //   projectOfficer: "",
    //   branchDto: "",
    //   sbuDto: "",
    // });
  }

  function handel(e) {
    console.log("E", e.target.value);
    const newdata = { ...data };
    newdata[e.target.id] = e.target.value;
    setData(newdata);
    console.log(newdata);
  }
  // ************************************************************ //
  const [projects, setProjects] = useState([]);
  const [branches, setBranches] = useState([]);
  const [sbus, setSbus] = useState([]);

  useEffect(() => {
    getProjects();
    getBranches();
    getSbus();
  }, []);

  const getProjects = () => {
    projectJsx
      .getAllProjects()
      .then((response) => {
        console.log("My Fetched data", response.responseDto);
        setProjects(response.responseDto.payload);
      })
      .catch(function (ex) {
        console.log("Response parsing failed. Error: ", ex);
      });
  };

  const getBranches = () => {
    projectJsx
      .getAllBranchesList()
      .then((response) => {
        console.log("Branch data", response.responseDto);
        setBranches(response.responseDto.payload);
      })
      .catch(function (ex) {
        console.log("Response parsing failed. Error: ", ex);
      });
  };

  const getSbus = () => {
    projectJsx
      .getAllSBU()
      .then((response) => {
        console.log("SBU data", response.responseDto);
        setSbus(response.responseDto.payload);
      })
      .catch(function (ex) {
        console.log("Response parsing failed. Error: ", ex);
      });
  };

  const [sub, setSub] = useState();

  useEffect(() => {
    setSub(parseFloat(data.totalExtent)-(parseFloat( data.reservationExtent) +parseFloat( data.roadWaysExtent )+ parseFloat(data.otherReservationsExtent)) );
  }, [data]);

  function handleChange(e) {
    console.log("GetValue", e.target.value);
    console.log("GetValue", e.target.name);
  }
  // ************************************************************ //
  return (
    <div>
      {/* <createScript isHydrating={true} type="text/javascript" src="creatProject.js" />; */}
      <div>
        <SideNav />
        <div style={{ width: "83%", float: "left" }}>
          <SearchBar />
          <div
            id="page-wrapper"
            class="gray-bg dashbard-1"
            style={{ width: "100%" }}
          >
            <div class="row wrapper border-bottom white-bg page-heading">
              <div class="col-lg-10">
                <h2>Create Project</h2>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="home.html">Home</a>
                  </li>
                  <li class="breadcrumb-item">
                    <a>Create Project</a>
                  </li>
                  <li class="breadcrumb-item active">
                    <strong>Create Project</strong>
                  </li>
                </ol>
              </div>
            </div>
            <form onSubmit={(e) => submit(e)}>
              <div class="wrapper wrapper-content animated fadeInRight">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="ibox">
                        <div class="ibox-content">
                          <div class="form-group row">
                            {/* {options.map((myOption, key) => {
                                                            { console.log("OPTION LOG", key) }
                                                            // <div style={{ backgroundColor: "red", width: 200, height: 200 }}>ahhhh</div>
                                                            return (
                                                                <option value={myOption.projectName}>{myOption.projectName}</option>
                                                            )
                                                        })} */}
                            {/* <label for="" class="col-sm-2 col-form-label">Select Land</label>
                                                        <div class="col-sm-4">
                                                            <select class="select2 form-control" id="land" name="land" required>
                                                            {projects.map(project => (
                                                                <option value="Select One" placeholder="Select One" >
                                                                    {project.projectName}
                                                                </option>
                                                            ))}
                                                            </select>
                                                        </div> */}

                            <label for="" class="col-sm-2 col-form-label">
                              Name the Project
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.projectName}
                                type="text"
                                class="form-control form-control-sm"
                                id="projectName"
                                name="projectName"
                                required
                              />
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              Branch
                            </label>
                            <div class="col-sm-4">
                              <select
                                class="form-control"
                                id="branchDto"
                                name="branchDto"
                                onChange={(e) => handel(e)}
                              >
                                <option>--- Select ---</option>
                                {branches.map((branch) => (
                                  <option value={branch.id}>
                                    {branch.branchName}
                                  </option>
                                ))}
                              </select>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Project Officer
                            </label>
                            <div class="col-sm-4">
                              <select
                                class="form-control"
                                id="projectOfficer"
                                name="projectOfficer"
                                onChange={(e) => handel(e)}
                              >
                                <option> --- Select ---</option>
                                {projects.map((project) => (
                                  <option value={project.id}>
                                    {project.projectName}
                                  </option>
                                ))}
                              </select>
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              SBU
                            </label>
                            <div class="col-sm-4">
                              <select
                                class="form-control"
                                id="sbuDto"
                                name="sbuDto"
                                onChange={(e) => handel(e)}
                              >
                                <option> --- Select ---</option>
                                {sbus.map((sbu) => (
                                  <option value={sbu.id}>{sbu.sbuName}</option>
                                ))}
                              </select>
                            </div>
                          </div>

                          <hr />
                          <br />

                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Extent Total
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.totalExtent}
                                type="number"
                                class="form-control form-control-sm"
                                id="totalExtent"
                                name="extentTotal"
                                min="0"
                                step="any"
                                required
                              />
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              Reservation<span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.reservationExtent}
                                type="number"
                                class="form-control form-control-sm"
                                id="reservationExtent"
                                name="reservation"
                                min="0"
                                step="any"
                                required
                              />
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Road and Facility
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.roadWaysExtent}
                                type="number"
                                class="form-control form-control-sm"
                                id="roadWaysExtent"
                                name="facility"
                                min="0"
                                step="any"
                                required
                              />
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              Other Reservation
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.otherReservationsExtent}
                                type="number"
                                class="form-control form-control-sm"
                                id="otherReservationsExtent"
                                name="otherReservationsExtent"
                                min="0"
                                step="any"
                              />
                            </div>
                            <br/><br/><br/>
                            <label for="" class="col-sm-2 col-form-label">
                              Sellable Extent
                            </label>
                            <div class="col-sm-4">
                              <span
                                class="form-control form-control-sm"
                                id="sellableExtent"
                                name="sellableExtent"
                                placeholder="hdkj"
                                value={sub}
                              >
                                {sub}
                              </span>
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Start Date<span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <div class="form-group" id="data_1">
                                <input
                                  onChange={(e) => handel(e)}
                                  value={data.startDate}
                                  type="date"
                                  class="form-control form-control-sm"
                                  id="startDate"
                                  required
                                />
                              </div>
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              Expected End Date
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <div class="form-group" id="data_1">
                                <input
                                  onChange={(e) => handel(e)}
                                  value={data.endDate}
                                  type="date"
                                  class="form-control form-control-sm"
                                  id="endDate"
                                  required
                                />
                              </div>
                            </div>
                          </div>

                          <hr />
                          <br />

                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Budgeted Cost
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.budgetedCost}
                                type="number"
                                class="form-control form-control-sm"
                                id="budgetedCost"
                                name="budgetedCost"
                                min="0"
                                required
                              />
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              Budgeted COC
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.costOfCapital}
                                type="number"
                                class="form-control form-control-sm"
                                id="costOfCapital"
                                name="budgetedCOC"
                                min="0"
                                required
                              />
                            </div>
                          </div>

                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Budgeted Revenue
                              <span style={{ color: "red" }}>*</span>
                            </label>
                            <div class="col-sm-4">
                              <input
                                onChange={(e) => handel(e)}
                                value={data.budgetedRevenue}
                                type="number"
                                class="form-control form-control-sm"
                                id="budgetedRevenue"
                                name="budgetedRevenue"
                                min="0"
                                required
                              />
                            </div>
                            <label for="" class="col-sm-2 col-form-label">
                              Block out Plan
                            </label>
                            <div class="col-sm-4">
                              <div class="custom-file">
                                <input
                                  id="logo"
                                  type="file"
                                  class="custom-file-input"
                                />
                                <label for="logo" class="custom-file-label">
                                  Choose file...
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">
                              Description
                            </label>
                            <div class="col-sm-4">
                              <textarea
                                rows="3"
                                onChange={(e) => handel(e)}
                                value={data.description}
                                type="text"
                                class="form-control"
                                id="description"
                                name="description"
                              ></textarea>
                            </div>
                          </div>
                          <button class="btn btn-primary btn-sm ">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>

            <div class="footer">
              <div class="float-right">
                10GB of <strong>250GB</strong> Free.
              </div>
              <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2018
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CreateProject;
