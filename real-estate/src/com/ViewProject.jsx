import React, { useEffect, useState } from "react";
import projectJsx from "../com/Services/Project";
import { FaEye } from "react-icons/fa";
import { RiDeleteBin6Line } from "react-icons/all";
import ReactPaginate from 'react-paginate';
import ReactDOM from 'react-dom';
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";

const ViewProject = () => {

    const [projects, setProjects] = useState([])

    useEffect(() => {
        getProjects()
    }, [])

    const getProjects = () => {
        projectJsx.getAllProjects().then((response) => {
            console.log("My Fetched data", response.responseDto)
            setProjects(response.responseDto.payload)
        })
            .catch(function (ex) {
                console.log('Response parsing failed. Error: ', ex);
            });;
    };

    function handleChange(e) {
        console.log("GetValue", e.target.value)
        console.log("GetValue", e.target.name)
    }

    return (

        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
                <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <table class="styled-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Project Name</th>
                                <th>Total Extent</th>
                                <th>Reservation Extent</th>
                                <th>Road Ways Extent</th>
                                <th>Other Reservations Extent</th>
                                <th>Sellable Extent</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Budgeted Cost</th>
                                <th>Cost of Capital</th>
                                <th>Project Officer</th>
                                <th>Description</th>
                                <th>Created User</th>
                                <th>Created Date</th>
                                <th>Created Time</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {projects.map(project => (
                                <tr>
                                    <td>{project.id}</td>
                                    <td>{project.projectName}</td>
                                    <td>{project.totalExtent}</td>
                                    <td>{project.reservationExtent}</td>
                                    <td>{project.roadWaysExtent}</td>
                                    <td>{project.otherReservationsExtent}</td>
                                    <td>{project.sellableExtent}</td>
                                    <td>{project.startDate}</td>
                                    <td>{project.endDate}</td>
                                    <td>{project.budgetedCost}</td>
                                    <td>{project.costOfCapital}</td>
                                    <td>{project.projectOfficer}</td>
                                    <td>{project.description}</td>
                                    <td>{project.savedUser}</td>
                                    <td>{project.savedDate}</td>
                                    <td>{project.savedTime}</td>
                                    <td>{project.reStatusDto.status}</td>
                                    <td style={{ width: '100px' }}>
                                        <button class="table-button">
                                            <FaEye class="table-icon" style={{color:"green"}}/>
                                        </button>
                                        <button class="table-button" style={{ float: 'right' }}>
                                            <RiDeleteBin6Line class="table-icon" style={{color:"red"}}/>
                                        </button>
                                    </td>
                                </tr>
                            ))
                            }
                        </tbody>
                    </table>
                </div>
            </div >
        </div>
    )
};
export default ViewProject;