import React from "react";
// import profile_small from "./img/profile_small.jpg";
import p_big1 from "./img/p_big1.jpg";
import p_big2 from "./img/p_big2.jpg";
import p_big3 from "./img/p_big3.jpg";
import SearchBar from "./SearchBar";
import SideNav from "./SideNav";

const interactionAdding = () => {
    return (
        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
                <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <div class="row wrapper border-bottom white-bg page-heading">
                        <div class="col-lg-10">
                            <h2>Interaction Adding</h2>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a>Interaction Adding</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <strong>Interaction Adding</strong>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <div class="form-group row">
                                                <label for="" class="col-sm-1 col-form-label">Customer</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="customer" name="customer" />
                                                </div>
                                                <label for="" class="col-sm-2 col-form-label">Sale Transaction No</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="transactionNo" name="transactionNo" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-1 col-form-label">Project</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="project" name="project" />
                                                </div>
                                                <label for="" class="col-sm-2 col-form-label">LOT</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="lot" name="lot" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-11">
                                                    <button type="button" class="btn btn-success btn-sm pull-right">Submit</button>
                                                </div>
                                            </div>
                                            <br />

                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-2 col-form-label font-bold">Sale Transaction :</label>
                                                        <div class="col-sm-4">
                                                            <label for="" class="col-form-label"></label>
                                                        </div>
                                                        <label for="" class="col-sm-2 col-form-label font-bold">Project :</label>
                                                        <div class="col-sm-4">
                                                            <label for="" class="col-form-label"></label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-2 col-form-label font-bold">Customer :</label>
                                                        <div class="col-sm-4">
                                                            <label for="" class="col-form-label"></label>
                                                        </div>
                                                        <label for="" class="col-sm-2 col-form-label font-bold">LOT :</label>
                                                        <div class="col-sm-4">
                                                            <label for="" class="col-form-label"></label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="" class="col-sm-2 col-form-label font-bold">Start Date :</label>
                                                        <div class="col-sm-4">
                                                            <label for="" class="col-form-label"></label>
                                                        </div>
                                                        <label for="" class="col-sm-2 col-form-label font-bold">Expected End Date :</label>
                                                        <div class="col-sm-4">
                                                            <label for="" class="col-form-label"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="ibox">
                                                        <div class="slick_demo_1">
                                                            <div>
                                                                <div class="ibox-content">
                                                                    <img src={p_big1} style={{ width: "100%" }} />
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="ibox-content">
                                                                    <img src={p_big2} style={{ width: "100%" }} />
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="ibox-content">
                                                                    <img src={p_big3} style={{ width: "100%" }} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <br />

                                            <div class="tabs-container">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li><a class="nav-link active" data-toggle="tab" href="#tab-1"> Schedule</a></li>
                                                    <li><a class="nav-link" data-toggle="tab" href="#tab-2">Receipts</a></li>
                                                    <li><a class="nav-link" data-toggle="tab" href="#tab-3">Ledger</a></li>
                                                    <li><a class="nav-link" data-toggle="tab" href="#tab-4">Interactions</a></li>
                                                    <li><a class="nav-link" data-toggle="tab" href="#tab-5">Documents</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" id="tab-1" class="tab-pane active">
                                                        <div class="panel-body">

                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                    <thead>
                                                                        <tr>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" id="tab-2" class="tab-pane">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                    <thead>
                                                                        <tr>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" id="tab-3" class="tab-pane">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                    <thead>
                                                                        <tr>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" id="tab-4" class="tab-pane">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Date</th>
                                                                            <th>Type</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>
                                                                                <button type="button" class="btn btn-warning btn-sm">Comment</button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" id="tab-5" class="tab-pane">
                                                        <div class="panel-body">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="footer">
                        <div class="float-right">
                            10GB of <strong>250GB</strong> Free.
                        </div>
                        <div>
                            <strong>Copyright</strong> Example Company &copy; 2014-2018
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default interactionAdding;