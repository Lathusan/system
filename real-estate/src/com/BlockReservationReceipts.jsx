import React from "react";
import { Style } from "react-style-tag";
import SearchBar from "./SearchBar";
import SideNav from "./SideNav";

const blockReservationReceipts = () => {
    return (
        <div>
            <style>{`
            @page {size: A5 landscape }
            .smallText{
                                    font-size: small;
                                }
                                body{
                                    background-color: #CCC;
                                    font-size: small;
                                }
                                .table-condensed>thead>tr>th, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>tbody>tr>td, .table-condensed>tfoot>tr>td{
                                    padding: 3px;
                                }
                                .sheet{
                                    padding-top: 1cm;
                                    padding-left: 2cm;
                                    padding-right: 2cm;
                                    padding-bottom: 1cm;
                                }
                                @media print {
                                    .avoid{page-break-inside: avoid;}
                                    .sheet{
                                        padding-top: 1cm;
                                        padding-left: 2cm;
                                        padding-right: 2cm;
                                        padding-bottom: 1cm;

                                    }
                                    page[size="A5"][layout="landscape"] {
                                        width: 210mm;
                                        height: 148mm;  
                                    }
                                    .noPrint{
                                        display: none;
                                    }
                                }

                                table.table{
                                    border:1px solid #fff;
                                }
                                table.table > thead > tr > th{
                                    border:1px solid #fff;
                                }
                                table.table > tbody > tr > td{
                                    border:1px solid #fff ;
                                }
                                table.table-bordered{
                                    border:1px solid #aaa;

                                }
                                table.table-bordered > thead > tr > th{
                                    border:1px solid #aaa;
                                }
                                table.table-bordered > tbody > tr > td{
                                    border:1px solid #aaa ;
                                }
                                `}</style>
            <div>
                <SideNav />
                <div style={{ width: '83%', float: 'left' }}>
                    <SearchBar />

                    <input type="button" onclick="window.print();" class="form-control btn btn-success noPrint" value="Click Here To Print This Page" />
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12">
                                <center>
                                    <h1>Summit Capital</h1>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <table style={{ width: "100%" }} />
                            <tbody>
                                <tr style={{ height: "30px" }}>
                                    <td>Sale Acoount No :&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="text" class="form-control" disabled="" /></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style={{ height: "30px" }}>
                                    <td>Project :</td>
                                    <td><input type="text" class="form-control" /></td>
                                    <td>LOT NO :</td>
                                    <td></td>
                                </tr>
                                <tr style={{ height: "30px" }}>
                                    <td>Non Refundable Advance :</td>
                                    <td>xxx</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style={{ height: "30px" }}>
                                    <td>Advance :</td>
                                    <td>xxx</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style={{ height: "30px" }}>
                                    <td>Total Payment :</td>
                                    <td><input type="text" class="form-control" /></td>
                                    <td> </td>
                                    <td></td>
                                </tr>
                                <tr style={{ height: "50px" }}>
                                    <td>Customer Name <br /> <br />
                                        <textarea rows="2" class="form-control"></textarea>
                                    </td>
                                    <td></td>
                                    <td> Customer Address <br /> <br />
                                        <textarea rows="2" class="form-control"></textarea>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default blockReservationReceipts;