import React, { useEffect, useState } from "react";
import SearchBar from "./SearchBar";
import SideNav from "./SideNav";
import projectJsx from "../com/Services/Project";
import blockCreation from "../com/Services/BlockCreation";
import Axios from "axios";

const BlockReservation = () => {

    const [projects, setProjects] = useState([])
    const [blocks, setBlocks] = useState([])

    useEffect(() => {
        getProjects()
        getBlocks()
    }, [])

    const getProjects = () => {
        projectJsx.getAllProjects().then((response) => {
            console.log("Project data", response.responseDto)
            setProjects(response.responseDto.payload)
        })
            .catch(function (ex) {
                console.log('Response parsing failed. Error: ', ex);
            });;
    };

    const getBlocks = () => {
        blockCreation.getAllBlocks().then((response) => {
            console.log("Blocks data", response.responseDto)
            setBlocks(response.responseDto.payload)
        })
            .catch(function (ex) {
                console.log('Response parsing failed. Error: ', ex);
            });;
    };

    const url = "http://localhost:8081/reProjectDetails/save"
    const [data, setData] = useState({
        projectName: "",
        reservationExtent: "",
        otherReservationsExtent: "",
        roadWaysExtent: "",
    })

    function submit(e) {
        e.preventDefault();
        Axios.post(url, {
            projectName: data.projectName,
            reservationExtent: data.reservationExtent,
            otherReservationsExtent: data.otherReservationsExtent,
            roadWaysExtent: data.roadWaysExtent,
            startDate: data.startDate,
        })
            .then(res => {
                console.log(res.data)
            })
    }

    function handel(e) {
        const newdata = { ...data }
        newdata[e.target.id] = e.target.value
        setData(newdata)
        console.log(newdata)
    }

    return (

        <div>
            <SideNav />
            <div style={{ width: '83%', float: 'left' }}>
                <SearchBar />
                <div id="page-wrapper" class="gray-bg dashbard-1" style={{ width: '100%' }}>
                    <div class="row wrapper border-bottom white-bg page-heading">
                        <div class="col-lg-10">
                            <h2>Block Reservation</h2>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="home.html">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a>Block Reservation</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    <strong>Block Reservation</strong>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="wrapper wrapper-content animated fadeInRight">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <div class="form-group row">
                                                <label for="" class="col-sm-1 col-form-label">Project</label>
                                                <div class="col-sm-4">
                                                    <select class="select2 form-control" id="project" name="project">
                                                    <option>--- Select ---</option>
                                                    {projects.map(project => (
                                                        <option value={project.id}>{project.projectName}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                                <label for="" class="col-sm-2 col-form-label">Block</label>
                                                <div class="col-sm-4">
                                                    <select class="select2 form-control" id="block" name="block">
                                                    <option>--- Select ---</option>
                                                    {blocks.map(block => (
                                                        <option value={block.id}>{block.id}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-1 col-form-label">Customer</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="customer" name="customer" />
                                                </div>
                                                <label for="" class="col-sm-2 col-form-label">Marketing Officer</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="marketingOfficer" name="marketingOfficer" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-1 col-form-label">Introducer</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="introducer" name="introducer" />
                                                </div>
                                                <label for="" class="col-sm-2 col-form-label">Sale Config Type</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="configType" name="configType" />
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-sm-1 col-form-label">Non Refundable Advance</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control form-control-sm" id="refundableAdvance" name="refundableAdvance" disabled="" />
                                                </div>
                                                <label for="" class="col-sm-2 col-form-label">Payment</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Rs</span>
                                                        </div>
                                                        <input type="text" class="form-control" aria-label="" aria-describedby="basic-addon1" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-sm-11">
                                                    <button type="button" class="btn btn-primary btn-sm pull-right">Save</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="footer">
                        <div class="float-right">
                            10GB of <strong>250GB</strong> Free.
                        </div>
                        <div>
                            <strong>Copyright</strong> Example Company &copy; 2014-2018
                        </div>
                    </div>

                </div>
            </div>
        </div>

    );
};

export default BlockReservation;
{/* script file is miising */ }