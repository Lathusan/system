import React, { useEffect, useState } from "react";
import SideNav from "./SideNav";
import SearchBar from "./SearchBar";
import Axios from "axios";
import swal from 'sweetalert';

const Branch = () => {

  const url = "http://localhost:8081/branch/save";
  const [data, setData] = useState({
    branchName: "",
    address: "",
    contactNumber: "",
    emailAddress: "",
  });

  function submit(e) {
    e.preventDefault();
    Axios.post(url, {
      branchName: data.branchName,
      address: data.address,
      contactNumber: data.contactNumber,
      emailAddress: data.emailAddress,
    }).then((res) => {
      console.log(res.data);
    });
    swal({
      title: "Success",
      text: "Information save successfully!",
      icon: "success",
      timer: 1500,
      buttons: false,      
    });
    window.location.reload(false);
  }

  function handel(e) {
    const newdata = { ...data };
    newdata[e.target.id] = e.target.value;
    setData(newdata);
    console.log(newdata);
  }

  return (
    <div>
      <SideNav />
      <div style={{ width: "83%", float: "left" }}>
        <SearchBar />
        <div
          id="page-wrapper"
          class="gray-bg dashbard-1"
          style={{ width: "100%" }}
        >
          <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
              <h2>Create Branch</h2>
              <ol class="breadcrumb">
                <li class="breadcrumb-item">
                  <a href="home.html">Home</a>
                </li>
                <li class="breadcrumb-item">
                  <a>Create Branch</a>
                </li>
                <li class="breadcrumb-item active">
                  <strong>Create Branch</strong>
                </li>
              </ol>
            </div>
          </div>

          <form onSubmit={(e) => submit(e)}>
            <div class="wrapper wrapper-content animated fadeInRight">
              <div class="col-lg-12">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="ibox">
                      <div class="ibox-content">
                        <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">
                            Branch Name<span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.branchName}
                              type="text"
                              class="form-control form-control-sm"
                              id="branchName"
                              name="branchName"
                              required
                            />
                          </div>
                          <label for="" class="col-sm-2 col-form-label">
                            Contact Number
                            <span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.contactNumber}
                              type="text"
                              class="form-control form-control-sm"
                              id="contactNumber"
                              name="contactNumber"
                              required
                            />
                          </div>
                        </div>

                        <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label">
                            Address <span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <textarea
                              onChange={(e) => handel(e)}
                              value={data.address}
                              rows="3"
                              type="text"
                              class="form-control"
                              id="address"
                              name="address"
                              required
                            ></textarea>
                          </div>

                          <label for="" class="col-sm-2 col-form-label">
                            Email<span style={{ color: "red" }}> *</span>
                          </label>
                          <div class="col-sm-4">
                            <input
                              onChange={(e) => handel(e)}
                              value={data.emailAddress}
                              type="email"
                              class="form-control form-control-sm"
                              id="emailAddress"
                              name="emailAddress"
                              min="0"
                              required
                            />
                          </div>
                        </div>
                        <button class="btn btn-primary btn-sm">Save</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Branch;
