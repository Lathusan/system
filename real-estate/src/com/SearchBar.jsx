import React, { useEffect, useState } from "react";
import { FaBars } from "react-icons/fa/index";
import { useHistory } from "react-router-dom";
import { AppString } from "./Const";
import Axios from "axios";

const SearchBar = () => {

     const history = useHistory();
    // const url = "http://localhost:8080/user/loginAuth"
    const [data, setData] = useState({
        email: "",
        password: "",
    })
    // useEffect(() => {
    //     if (localStorage.getItem(AppString.USER) == 'true') {
    //        // localStorage.setItem(AppString.USER, "false");
    //         history.push("/login");
    //     }
    // }, [])

    // function submit(e) {
    //     e.preventDefault();
    //     Axios.post(url, {
    //         email: data.email,
    //         password: data.password,
    //     })
    //        .then(res => {
    //             console.log("Log", data.responseDto)
    //             ;
    //             if (localStorage.setItem(AppString.USER) == 'false') {
    //                console.log("True")
    //                 history.push("/login");
    //                 localStorage.setItem(AppString.USER, "false");
    //             }
    //             else {
    //                 alert("Invalide Login!!!")
    //             }
    //        })
    // }

    // function handel(e) {
    //     const newdata = { ...data }
    //     newdata[e.target.id] = e.target.value
    //     setData(newdata)
    //     console.log(newdata)
    // }

    return (
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style={{ marginBottom: "0" }}>
                    <div class="navbar-header">
                        {/* <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> */}
                        <FaBars class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" style={{ width: '30px', height: '25px' }} />
                        <form role="search" class="navbar-form-custom" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search" />
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li style={{ padding: "20px" }}>
                            <span class="m-r-sm text-muted welcome-message">Welcome to REAL ESTATE.</span>
                        </li>

                        <li>
                            <a href="login" >
                                <i class="fa fa-sign-out"></i> <button>Log out</button> {/*onSubmit={(e) => submit(e)}*/}
                            </a>
                        </li>

                    </ul>
                </nav>
            </div>
    );
};

export default SearchBar;