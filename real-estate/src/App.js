import React from "react";
// import { BrowserRouter as Router, Route } from "react-router-dom"; // switch
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import axios from "axios";
import Login from "./com/Login";
import Dashboard from "./com/Dashboard";
import CreateProject from "./com/CreateProject";
import CreateCustomer from "./com/CreateCustomer";
import BlockReservation from "./com/BlockReservation";
import BlockReservationReceipts from "./com/BlockReservationReceipts";
import Receipting from "./com/Receipting";
import SaleTypeConfiguration from "./com/SaleTypeConfiguration";
import InteractionAdding from "./com/InteractionAdding";
import BlockOutPlan from "./com/BlockOutPlan";
import SideNav from "./com/SideNav";
import ViewProject from "./com/ViewProject";
import SearchBar from "./com/SearchBar";
import ViewBlock from "./com/ViewBlock";
import { AppString } from "./com/Const";
import Branch from "./com/Branch";
import PaginationTable from "./com/PaginationTable";
import Land from "./com/Land";

function App() {
  return (
    <div className="App">

      <Router>
        <div>
          {/* <Login /> */}
          {/* {localStorage.getItem(AppString.USER) == 'true' ? (
            <SideNav />
            //  {window.location.pathname != '/searchBar' && <SearchBar />}
          ) : null} */}

          <Switch>
            
            <Route exact path='/' component={Login} />
            <Route path='/dashboard' component={Dashboard} />
            <Route path='/createProject' component={CreateProject} />
            <Route path='/createCustomer' component={CreateCustomer} />
            <Route path='/blockReservation' component={BlockReservation} />
            <Route path='/blockReservationReceipts' component={BlockReservationReceipts} />
            <Route path='/receipting' component={Receipting} />
            <Route path='/saleTypeConfiguration' component={SaleTypeConfiguration} />
            <Route path='/interactionAdding' component={InteractionAdding} />
            <Route path='/blockOutPlan' component={BlockOutPlan} />
            <Route path='/sideNav' component={SideNav} />
            <Route path='/viewProject' component={ViewProject} />
            <Route path='/viewBlock' component={ViewBlock} />
            <Route path='/branch' component={Branch} />
            <Route path='/PaginationTable' component={PaginationTable} />
            <Route path={'/land'} component={Land} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
